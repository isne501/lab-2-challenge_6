#ifndef Graph_H
#define Graph_H
#include <list>
#include <iostream>

using namespace std;

class Node;

class Edge
{
public:
	Edge(Node* source, Node* dest, int weight) //constructor
	{
		Source = source;
		Destination = dest;
		Weight = weight;
	}
	Node* getSource() { return Source; } //return the pointer that point to source
	Node* getDestination() { return Destination; }//return the pointer that point to Destination
	int getWeight() { return Weight; }//return weight value
private:
	Node* Source;
	Node* Destination;
	int Weight;
};

class Node
{
public:
	
	void addEdge(Node* dest, int weight) //add edge between vertice
	{
		Edge listedge(this, dest, weight);
		edge.push_back(listedge); //push edge to list 
	}

	void printEdge() //print edge
	{
		cout << "Vertice : " << getName() << endl;
		if (edge.empty()) //if vertice don't have edge to any Destination
		{
			cout << " No Destination" << endl;;
		}
		for (auto it = edge.begin(); it != edge.end(); it++) //if vertice have edge to any Destination loop for output every edge
		{
			Edge  pair = *it;
			cout << " To Destination " << pair.getDestination()->getName() << " with weight " << pair.getWeight() << endl;
		}
	}
	void setName(char Name) {  name = Name; } //set name of vertice
	char getName() { return name; } //get the name of vertice
	list<Edge> getEdges() { return edge; } //return the list of edge each vertice

private:
	char name;
	list<Edge> edge;
};

class Graph
{
public:

	void printGraph() //print graph
	{
		for (auto it = vertice.begin(); it != vertice.end(); it++)
		{
			Node vertice = *it;
			vertice.printEdge(); //print edge of each node
		}
	}

	void GrapharrayTolist(int **arr, int vertice_size) //change adjacency matrix to list
	{
		Node verticeName;

		for (int i = 0; i < vertice_size; i++)
		{
			verticeName.setName(65+i); //assign name alphabet code A begin with 65 
			vertice.push_back(verticeName); //push the name of vertice
		}

		int i = 0; //for row of array
		for (auto it = vertice.begin(); it != vertice.end(); it++)
		{
			for (int j = 0; j < vertice_size; j++)
			{
				Node *dest = new Node; //create new node that will be Destination
				Node &source = *it;
				if (arr[i][j] == 0) //if weight equal to 0 continue the loop
				{
					continue;
				}
				else {
					dest->setName(65 + j); //assign name alphabet code A begin with 65 
					source.addEdge(dest, arr[i][j]); //addedge between source and Destination with weight equal to arr[i][j]
				}
			}
			i++; //change row
		}
	}

	bool isPseudograph()
	{
		//loop to check every vertice
		for (auto it = vertice.begin(); it != vertice.end(); it++)
		{
			Node vertice = *it;
			list <Edge> listedges = vertice.getEdges(); //get edge of vertice
			for (auto it2 = listedges.begin(); it2 != listedges.end(); it2++)//loop to check each edge of vertice
			{
				Node *source = it2->getSource(); //get source pointer of vertice
				Node *dest = it2->getDestination(); //get destnition pointer of vertice
	
				if (source->getName() == dest->getName() && it2->getWeight()!=0) ////if vertice has edge that connect with same source and destination with weight not equal to 0
				{
					return true;
				}
			}
		}
		return false;
	}

	bool isWeightgraph()
	{
		//loop to check every vertice
		for (auto it = vertice.begin(); it != vertice.end(); it++)
		{
			Node vertice = *it;
			list <Edge> listedges = vertice.getEdges(); //get edge of vertice
			for (auto it2 = listedges.begin(); it2 != listedges.end(); it2++) //loop to check each edge of vertice
			{
				if (it2->getWeight() != 0) //if iterator that point to list of edge if the weight not equal to zero that mean weight graph
				{
					return true;
				}
			}
		}
		return false;
	}

	bool isCompletegraph()
	{
		int i = 0;
		//loop to check every vertice
		for (auto it = vertice.begin(); it != vertice.end(); it++)
		{
			Node vertice = *it;
			list <Edge> listedges = vertice.getEdges(); //get edge of vertice
			for (auto it2 = listedges.begin(); it2 != listedges.end(); it2++)
			{
				Node *source = it2->getSource(); //get source pointer of vertice
				Node *dest = it2->getDestination(); //get destnition pointer of vertice
				i++;
				if (source->getName() == dest->getName())
				{
					i--; //minus the edge that psuedo
				}
			}
		}

		if (i == vertice.size()*(vertice.size()-1) )//formula to check complete graph
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	
	bool isDigraph() 
	{
		//loop for fisrt vertice
		for (auto it = vertice.begin(); it != vertice.end(); it++)
		{
			Node vertice1 = *it;
			list <Edge> listedges1 = vertice1.getEdges(); //get edge of first vertice
			
			//loop for fisrt vertice each edge
			for (auto it2 = listedges1.begin(); it2 != listedges1.end(); it2++)
			{
				Node *source1 = it2->getSource(); //get source pointer of first vertice
				Node *dest1 = it2->getDestination(); //get destnition pointer of first vertice
	
				//loop for second vertice
				for (auto it3 = vertice.begin(); it3 != vertice.end(); it3++) 
				{
					Node vertice2 = *it3;
				
					//check if it's same vertice or not (same vertice can't be compare)
					if (vertice2.getName() != vertice1.getName()) 
					{
						list <Edge> listedges2 = vertice2.getEdges(); //get edge of second vertice
						for (auto it4 = listedges2.begin(); it4 != listedges2.end(); it4++) 
						{ 
							
							Node * source2 = it4->getSource(); //get source pointer of second vertice
							Node * dest2 = it4->getDestination();//get destnition pointer of second vertice

							//if vertice 1 cango to vertice2 and vertice 2 can go to vertice 1 that mean it isn't directed graph
							if (source1->getName() == dest2->getName() && source2->getName() == dest1->getName() && (source2->getName()!= dest1->getName() || source2->getName() != dest2->getName()))
							{ 
								return false;
							}
						}	
					}
				}
			}
		}
		if (isWeightgraph() == false) //if emtpy list 
		{
			return false;
		}
		return true;
	}

	bool isMultigraph() //adjacency matrix can't represented the mutilgraph
	{
		return false;
	}
private:
	list<Node> vertice;
};
#endif
